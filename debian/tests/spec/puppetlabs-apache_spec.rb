require 'spec_helper'

describe "apache2 service is not enabled/started" do
  describe service('apache2') do
    it { should_not be_enabled }
    it { should_not be_running }
  end

  describe port(80) do
    it { should_not be_listening }
  end
end

describe "main apache class applies successfully" do
  describe command('systemd-run --service-type oneshot --unit puppet-apply --collect -- puppet apply --debug -e "include apache"') do
    its(:exit_status) { should eq 0 }
  end
end

describe "apache2 service is started and listening on port 80" do
  describe service('apache2') do
    it { should be_running }
  end

  describe port(80) do
    it { should be_listening }
  end
end
